# Angmap App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.4.
This project build using (recommended) [NODE](https://nodejs.org/en/blog/release/v12.11.0) version 12.11.0.

# Install Package

Run `npm install` to install package. installed package is needed before run the app.

## Running locally

Run `npm run start` to run the project. The run will open at <a href="localhost">localhost</a>.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).


## Live app
for experience in live app, the app can open at [angmap-app](https://mapdendy.netlify.app)
