import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutsRoutingModule } from './layouts-routing.module';
import { LayoutsComponent } from './layouts.component';
import { HeaderComponent } from './header/header.component';
import { SidebarLeftComponent } from './sidebar-left/sidebar-left.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  declarations: [LayoutsComponent, HeaderComponent, SidebarLeftComponent],
  exports: [
    LayoutsComponent
  ],
  imports: [
    CommonModule,
    LayoutsRoutingModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule
  ]
})
export class LayoutsModule { }
