import { Component, OnInit } from '@angular/core';
import {SidebarToggle} from "./sidebar-left/interface/sidebar.interface";

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.component.html',
  styleUrls: ['./layouts.component.sass']
})
export class LayoutsComponent implements OnInit {

  isCollapsed = false
  screenWidth = 0

  constructor() { }

  ngOnInit(): void {
  }

  onToggleSidebar(data: SidebarToggle): void {
    this.screenWidth = data.screenWidth
    this.isCollapsed = data.collapsed
  }

  getMainContainer(): string {
    let styleClass = ''
    if (this.isCollapsed && this.screenWidth > 768) {
      styleClass = 'container-trimmed'
    } else if (this.isCollapsed && this.screenWidth <= 768 && this.screenWidth > 0) {
      styleClass = 'container-md-screen'
    }

    return styleClass
  }

}
