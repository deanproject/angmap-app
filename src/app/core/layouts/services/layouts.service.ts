import {Injectable, OnInit} from '@angular/core';
import {MenuItem} from "../../../shared/models/menu-item.interfaces";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LayoutsService implements OnInit {

  model: MenuItem[] = [
    {
      label: 'Beranda',
      icon: 'home',
      routerLink: ['/'],
    },
    {
      label: 'About',
      icon: 'info',
      routerLink: ['/about'],
    }
  ]

  constructor() { }

  ngOnInit() {}

  public fetchSidebarItem(): Observable<Array<MenuItem>> {
    return of(this.model)
  }
}
