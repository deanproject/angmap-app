import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MenuItem} from "../../../shared/models/menu-item.interfaces";
import {LayoutsService} from "../services/layouts.service";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {SidebarToggle} from "./interface/sidebar.interface";

@Component({
  selector: 'app-sidebar-left',
  templateUrl: './sidebar-left.component.html',
  styleUrls: ['./sidebar-left.component.sass'],
  animations: [
    trigger('hambMenuX', [
      state('hambMenu', style({})),
      state('topX',
        style({
          transform: 'rotate(45deg)',
          transformOrigin: 'left',
          margin: '6px',
        })
      ),
      state('hide',
        style({
          opacity: 0,
        })
      ),
      state('bottomX',
        style({
          transform: 'rotate(-45deg)',
          transformOrigin: 'left',
          margin: '6px',
        })
      ),
      transition('* => *', [
        animate('.2s')
      ]),
    ]),
    trigger('fadeInOut',[
      transition(':enter', [
        style({opacity: 0}),
        animate('.35s',
          style({opacity: 1}))
      ]),
      transition(':leave', [
        style({opacity: 1}),
        animate('.35s',
          style({opacity: 0}))
      ]),
    ]),
  ],
})
export class SidebarLeftComponent implements OnInit {
  @Output() onToggleSidebar: EventEmitter<SidebarToggle> = new EventEmitter()
  collapsed = false
  model: MenuItem[] = []
  url: string
  isHambMenu = true
  screenWidth = 0

  constructor(
    private layoutServices: LayoutsService,
  ) { }

  ngOnInit(): void {
    this.screenWidth = window.innerWidth
    this.getMenu();
  }

  private getMenu() {
    this.layoutServices.fetchSidebarItem().subscribe((value: MenuItem[]) => {
      this.model = value
    })
  }

  public onClickHamb() {
    this.isHambMenu = !this.isHambMenu
    this.collapsed = !this.collapsed
    this.onToggleSidebar.emit({collapsed: this.collapsed, screenWidth: this.screenWidth})
  }
}
