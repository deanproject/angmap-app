import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import {HomeService} from "./home/services/home.service";


@NgModule({
  declarations: [HomeComponent, AboutComponent],
  imports: [
    CommonModule,
    PagesRoutingModule
  ],
  providers: [
    HomeService
  ]
})
export class PagesModule { }
