import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as L from 'leaflet';
import {HomeService} from "./services/home.service";
import {Cemetery, Properties} from "./models/cemetery.interface";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, AfterViewInit {
  private map

  constructor(private homeService: HomeService) { }

  ngOnInit(): void {
    this.getCemetery()
  }

  ngAfterViewInit(): void {
    this.initMap()
  }

  private initMap(): void {
    this.map = L.map('map')

    const googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
      maxZoom: 21,
      minZoom: 3,
      subdomains:['mt0','mt1','mt2','mt3']
    })
    googleStreets.addTo(this.map)
  }

  private doAddPolygon(latLng, properties) {
    let polygon = L.polygon(latLng, properties.status === 'Occupied' ? {} : {color: 'green'}).addTo(this.map)
    polygon.bindTooltip(this.addHTML(properties))
    this.map.fitBounds(polygon.getBounds())
  }

  private getCemetery() {
    this.homeService.getCemeteryData().subscribe({
      next: (result: Cemetery) => {
        result.features.forEach((item) => {
          item.geometry.coordinates.forEach((coorItem: []) => {
            coorItem.forEach(inCoorItem => {
              [inCoorItem[0], inCoorItem[1]] = [inCoorItem[1], inCoorItem[0]]
            })

            this.doAddPolygon(coorItem, item.properties)
          })
        })
      }
    })
  }

  private addHTML(properties: Properties) {
    let HTML= ''
    HTML += '<div>'
    HTML += properties.plot_id
    HTML += '</div>'
    properties.persons.forEach(item => {
      if (properties.persons.length > 0 && item.first_name) {
        HTML += '<div>'
        HTML += item.first_name + item.last_name
        HTML += '</div>'
      } else {
      }
    })
    return HTML
  }

}
