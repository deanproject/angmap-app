export interface Cemetery {
  type: string,
  features: Features[]
}

export interface Features {
  type: string,
  properties: Properties,
  geometry: Geometry
}

export interface Properties {
  plot_id: string,
  status: string,
  section: string,
  row: string,
  plot_no: string,
  cemetery_id: number,
  price: number,
  persons: Persons[],
  show_price: 0,
  cemetery_name: string,
  roi: []
}

export interface Geometry {
  type: string,
  coordinates: [],
}

export interface Persons {
  first_name: string,
  last_name: string,
  date_of_birth: string,
  date_of_death: string,
  age: string,
  returned_serviceman: boolean,
  is_featured: null,
  story: null,
  is_admin: null,
  life_chronicle: null
}
