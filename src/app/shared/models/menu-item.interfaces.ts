export interface MenuItem {
  icon?: string,
  label?: string,
  routerLink?: Array<string>;
}
